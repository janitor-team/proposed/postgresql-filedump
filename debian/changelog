postgresql-filedump (13.1-1) unstable; urgency=medium

  * New upstream version 13.1.

 -- Christoph Berg <myon@debian.org>  Mon, 02 Nov 2020 16:53:15 +0100

postgresql-filedump (13.0-1) unstable; urgency=medium

  * New upstream version 13.0.
  * Run testsuite at build-time.
  * R³: no.
  * DH 13.

 -- Christoph Berg <myon@debian.org>  Tue, 27 Oct 2020 15:36:58 +0100

postgresql-filedump (12.0-2) unstable; urgency=medium

  * tests: Depend on postgresql-server-dev-all for pg_config.

 -- Christoph Berg <myon@debian.org>  Fri, 29 Nov 2019 11:53:23 +0100

postgresql-filedump (12.0-1) unstable; urgency=medium

  * New upstream version.
  * Use (new) upstream testsuite instead of our own.
  * Add debian/gitlab-ci.yml.

 -- Christoph Berg <myon@debian.org>  Thu, 28 Nov 2019 15:04:55 +0100

postgresql-filedump (11.0-1) unstable; urgency=medium

  * New upstream version.
  * Pass CFLAGS as COPT so pgxs.mk picks them up.
  * Maintain PGBINDIR in debian/postinst and prerm automatically.
  * Run tests against all supported PostgreSQL versions.
  * Move maintainer address to team+postgresql@tracker.debian.org.

 -- Christoph Berg <myon@debian.org>  Sun, 02 Sep 2018 13:53:40 +0200

postgresql-filedump (10.1-1) unstable; urgency=medium

  * New upstream version.
  * Move packaging repository to salsa.debian.org
  * tests: Dump an actual table.

 -- Christoph Berg <christoph.berg@credativ.de>  Wed, 20 Jun 2018 12:09:31 +0200

postgresql-filedump (10.0-1) unstable; urgency=medium

  * New upstream release supporting PG10. (Closes: #878564)
  * Update Standards-Version and package URLs.
  * Re-enable PIE on 32bit archs.

 -- Christoph Berg <myon@debian.org>  Sun, 22 Oct 2017 20:12:17 +0200

postgresql-filedump (9.6.0-2) unstable; urgency=medium

  * Disable PIE on 32bit archs; cf. #797530.

 -- Christoph Berg <myon@debian.org>  Sat, 18 Mar 2017 14:28:04 +0100

postgresql-filedump (9.6.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Update watch file for https://wiki.postgresql.org/wiki/Pg_filedump

 -- Christoph Berg <christoph.berg@credativ.de>  Thu, 29 Sep 2016 13:32:10 +0200

postgresql-filedump (9.5.0-2) unstable; urgency=medium

  * Move pg_filedump to 9.5 directory, spotted by Sébastien Lardière.
  * Add simple adt test.

 -- Christoph Berg <myon@debian.org>  Sun, 15 May 2016 20:16:54 +0200

postgresql-filedump (9.5.0-1) unstable; urgency=medium

  * New upstream release.

 -- Christoph Berg <myon@debian.org>  Sat, 19 Mar 2016 18:18:48 +0100

postgresql-filedump (9.3.0-4) unstable; urgency=medium

  * Add patch by Satoshi Nagayasu to support 9.5. Closes: #818668.
  * Make PG version a variable in debian/rules.

 -- Christoph Berg <myon@debian.org>  Sat, 19 Mar 2016 17:38:18 +0100

postgresql-filedump (9.3.0-3) unstable; urgency=medium

  * Remove bogus quotes from CFLAGS in debian/rules to fix building for
    reproducible.debian.net.

 -- Christoph Berg <myon@debian.org>  Thu, 05 Feb 2015 22:58:31 +0100

postgresql-filedump (9.3.0-2) unstable; urgency=medium

  * Pull upstream patch to support page checksums.
  * Package is compatible with PostgreSQL 8.4..9.4.

 -- Christoph Berg <myon@debian.org>  Sun, 31 Aug 2014 12:07:38 +0200

postgresql-filedump (9.3.0-1) unstable; urgency=medium

  * Imported Upstream version 9.3.0
  * Canonicalized VCS information.
  * Added hardening options.

 -- Michael Meskes <meskes@debian.org>  Mon, 16 Jun 2014 14:00:41 +0200

postgresql-filedump (9.2.0-1) unstable; urgency=low

  [ Michael Meskes ]
  * Added VCS information
  * Fixed typo
  * Imported Upstream version 9.2.0
  * Current PostgreSQL vesion is 9.3, so we'll have to build against that one.
  * Bumped Standards-Version to 3.9.5, no changes needed.

  [ Christoph Berg ]
  * Pull patch from upstream to support PostgreSQL 9.3, closes: #732510.
  * Mention compatible PostgreSQL versions in description.
  * Update watch file to find all releases.
  * Use format 3.0; bump to dh compat 9.
  * Set team as maintainer, add myself as uploader.

 -- Christoph Berg <myon@debian.org>  Tue, 01 Apr 2014 23:32:24 +0200

postgresql-filedump (9.1.0-1) unstable; urgency=low

  * Update watch file to correctly show latest version.
  * Imported Upstream version 9.1.0
  * Bumped Standards-Version to 3.9.3, no changes needed.
  * Install into 9.1 tree.

 -- Michael Meskes <meskes@debian.org>  Tue, 13 Mar 2012 15:28:16 +0100

postgresql-filedump (9.0.0-1) unstable; urgency=low

  * Imported Upstream version 9.0.0
  * Compile against PostgreSQL 9.1, closes: #639476
  * Removed PostgreSQL version number from package name.
  * Bumped Standars-Version to 3.9.2, no changes needed.
  * Updated Homepage: field.
  * Updated copyright file.
  * Added source/format and watch file.

 -- Michael Meskes <meskes@debian.org>  Mon, 29 Aug 2011 12:29:04 +0200

postgresql-filedump-8.4 (8.4.0-1) unstable; urgency=low

  * New upstream version for new PostgreSQL version, closes: #559590
  * Bumped Standards-Version to 3.8.3, no changes needed.
  * Upgraded to debhelper 7.

 -- Michael Meskes <meskes@debian.org>  Sat, 05 Dec 2009 19:06:45 +0100

postgresql-filedump-8.3 (8.3-1) unstable; urgency=low

  * New upstream version for new PostgreSQL version.

 -- Michael Meskes <meskes@debian.org>  Sat, 22 Mar 2008 21:16:24 +0100

postgresql-filedump-8.2 (8.2-4) unstable; urgency=low

  * Bumped standards version to 3.7.3.
  * Bumped debhelper compat version to 5.
  * Fixed remaining lintian warnings.

 -- Michael Meskes <meskes@debian.org>  Sat, 22 Dec 2007 18:25:16 +0100

postgresql-filedump-8.2 (8.2-3) unstable; urgency=low

  * Made package co-exist with 8.1 version, closes: #423148

 -- Michael Meskes <meskes@debian.org>  Thu, 10 May 2007 11:24:56 +0200

postgresql-filedump-8.2 (8.2-2) unstable; urgency=low

  * Fixed copyright file to list all authors 

 -- Michael Meskes <meskes@debian.org>  Mon,  7 May 2007 16:12:28 +0200

postgresql-filedump-8.2 (8.2-1) unstable; urgency=low

  * New upstream version, closes: #419302

 -- Michael Meskes <meskes@debian.org>  Sun, 15 Apr 2007 11:35:57 +0200

postgresql-filedump-8.1 (8.1-1) unstable; urgency=low

  * Initial release, closes: #342366

 -- Michael Meskes <meskes@debian.org>  Wed,  7 Dec 2005 15:08:28 +0100

